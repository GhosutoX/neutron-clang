# Quick Info
    * Build Date : 2023-01-16
    * Clang Version : 16.0.0
    * Binutils Version : 2.39
    * Compiled Based : https://github.com/llvm/llvm-project/commit/f8d90971
    * Release: https://github.com/Neutron-Toolchains/clang-build-catalogue/releases/tag/16012023